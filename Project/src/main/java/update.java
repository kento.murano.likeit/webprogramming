
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class update
 */
@WebServlet("/update")
public class update extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public update() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		 

		HttpSession session = request.getSession();
		if (request.getSession().equals(null)) { 
			response.sendRedirect("Login");
		}
		 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
	      dispatcher.forward(request, response);
	    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		String loginId = request.getParameter("loginid");
		String password = request.getParameter("password");
		String password1 = request.getParameter("password1");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthday");
//        Date formatDate=Date.valueOf(birthdate);
		UserDao userdao = new UserDao();
		if(password == "" && password1 == "") {
			 User user1=userdao.updateWithoutPassword(loginId, name, birthDate);
			 request.setAttribute("User", user1);
		}
		User user = userdao.update(loginId, password, password1, name, birthDate);
		request.setAttribute("User", user);
		
		if (!password.equals(password1) || loginId.equals("") || name == null || birthDate.equals(null)) {
			//ログイン失敗したとき、値を画面に戻す（入力した値を表示できるようにする
			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			 request.setAttribute("loginId", loginId);
			 request.setAttribute("password", password);
			 request.setAttribute("password1", password1);
			 request.setAttribute("name", name);
			 request.setAttribute("birthday", birthDate);
			 
			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

			
			return; //処理途中でフォワード、リダイレクトする場合はリターンする
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);

	}
	}

