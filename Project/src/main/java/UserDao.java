
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;




public class UserDao {

  /**
   * ログインIDとパスワードに紐づくユーザ情報を返す S
   * 
   * @param loginId
   * @param password
   * @return
   */
  public User findByLoginInfo(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      return new User(loginIdData, nameData);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
  }

  /**
   * 全てのユーザ情報を取得する
   *
   * @return
   */
  public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      // TODO: 未実装：管理者以外を取得するようSQLを変更する
      String sql = "SELECT * FROM user";

      // SELECTを実行し、結果表を取得
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      // 結果表に格納されたレコードの内容を
      // Userインスタンスに設定し、ArrayListインスタンスに追加
      while (rs.next()) {
        int id = rs.getInt("id");
        String loginId = rs.getString("login_id");
        String name = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        boolean isAdmin = rs.getBoolean("is_admin");
        Timestamp createDate = rs.getTimestamp("create_date");
        Timestamp updateDate = rs.getTimestamp("update_date");
        User user =
            new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

        userList.add(user);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
    return userList;
  }

  public User add(String loginId, String password, String name,
      String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      String sql = "INSERT INTO user VALUES (?,?,?,?)";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      pStmt.setString(3, name);
      pStmt.setString(4, birthDate);

      int result = pStmt.executeUpdate();



    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  public User userDetail(String loginId, String password) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      pStmt.setString(2, password);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return null;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id");
      String nameData = rs.getString("name");
      Date birthDate = rs.getDate("birthdate");
      Timestamp createDate = rs.getTimestamp("createdate");
      Timestamp updateDate = rs.getTimestamp("updateDate");
      return new User(loginIdData, nameData, birthDate, createDate, updateDate);

    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
      }
    }
}

public User update(String loginid, String password, String password1, String name,String birthDate) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();


      String sql = "UPDATE user SET password = ? ,name=?,birth_date=? WHERE login_id=?";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, password);
      pStmt.setString(2, name);
      pStmt.setString(3, birthDate);
      pStmt.setString(4, loginid);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
    }
    return null;
  }

  public User delete(String loginid) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();


      String sql = "DELETE FROM user WHERE login_id = ?";
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginid);
      int result = pStmt.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
        }
      }
  }
    return null;
  }

  public boolean findByloginId(String loginId) {
    Connection conn = null;
    try {
      // データベースへ接続
      conn = DBManager.getConnection();

      // SELECT文を準備
      String sql = "SELECT * FROM user WHERE login_id = ?";

      // SELECTを実行し、結果表を取得
      PreparedStatement pStmt = conn.prepareStatement(sql);
      pStmt.setString(1, loginId);
      ResultSet rs = pStmt.executeQuery();

      // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
      if (!rs.next()) {
        return true;
      }

      // 必要なデータのみインスタンスのフィールドに追加
      String loginIdData = rs.getString("login_id"); 
      return false;

    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    } finally {
      // データベース切断
      if (conn != null) {
        try {
          conn.close();
        } catch (SQLException e) {
          e.printStackTrace();
          return false;
        }
      }
    }
  }
  public User updateWithoutPassword(String loginid, String name,String birthDate) {
	    Connection conn = null;
	    try {
	      // データベースへ接続
	      conn = DBManager.getConnection();


	      String sql = "UPDATE user SET name=?,birth_date=? WHERE login_id=?";
	      PreparedStatement pStmt = conn.prepareStatement(sql);
	      pStmt.setString(1, name);
	      pStmt.setString(2, birthDate);
	      pStmt.setString(3, loginid);
	      int result = pStmt.executeUpdate();

	    } catch (SQLException e) {
	      e.printStackTrace();
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	    }
	    return null;
	  }
  public User reserch(String loginid, String name,String birthDate) {
	    Connection conn = null;
	    try {
	      // データベースへ接続
	      conn = DBManager.getConnection();


	      String sql = "UPDATE user SET name=?,birth_date=? WHERE login_id=?";
	      PreparedStatement pStmt = conn.prepareStatement(sql);
	      pStmt.setString(1, name);
	      pStmt.setString(2, birthDate);
	      pStmt.setString(3, loginid);
	      int result = pStmt.executeUpdate();

	    } catch (SQLException e) {
	      e.printStackTrace();
	    } finally {
	      // データベース切断
	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (SQLException e) {
	          e.printStackTrace();
	        }
	      }
	    }
	    return null;
	  }
}


