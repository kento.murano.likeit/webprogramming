

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAdd")
public class UserAdd extends HttpServlet {
  private static final long serialVersionUID = 1L;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public UserAdd() {
    super();
  }

  /**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (request.getSession().equals(null)) { 
			response.sendRedirect("Login");
		}
		 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
	      dispatcher.forward(request, response);
	    }
	 
  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	  String loginId = request.getParameter("loginid");
      String password = request.getParameter("password");
      String password1 = request.getParameter("password1");
      String name = request.getParameter("name");
      String birthDate = request.getParameter("birthday");
      
      String passwordEncorder=util.PasswordEncorder.encordPassword(password,password1);
     
	       UserDao userdao=new UserDao();
           User user = userdao.add(loginId, password, name, birthDate);
           boolean findbyloginid=userdao.findByloginId(loginId);
          if(!password.equals(password1)|| findbyloginid||loginId.equals(null)||name.equals(null)||birthDate.equals(null)) {
        	  request.setAttribute("errMsg", "入力された内容は正しくありません。");
        	  
        	  request.setAttribute("loginId", loginId);
 			 request.setAttribute("password", password);
 			 request.setAttribute("password1", password1);
 			 request.setAttribute("name", name);
 			 request.setAttribute("birthday", birthDate);
 			 RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
 			dispatcher.forward(request, response);

 			
 			return; //処理途中でフォワード、リダイレクトする場合はリターンする
 		}

          
	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
           dispatcher.forward(request, response);
	    }
}


